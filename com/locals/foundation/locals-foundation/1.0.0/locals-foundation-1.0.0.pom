<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.locals.foundation</groupId>
    <artifactId>locals-foundation</artifactId>
    <packaging>pom</packaging>
    <version>1.0.0</version>


    <modules>
        <module>foundation-message-api</module>
        <module>foundation-message-provider</module>
        <module>foundation-common-provider</module>
        <module>foundation-common-api</module>
    </modules>



    <parent>
        <groupId>com.locals.framework</groupId>
        <artifactId>locals-framework</artifactId>
        <version>1.0.0</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <repositories>
        <repository>
            <id>alimaven</id>
            <name>aliyun maven</name>
            <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
        </repository>

        <!-- 码云私仓 -->
        <repository>
            <id>locals-maven-repositories</id>
            <url>https://gitee.com/locals-home/locals-maven-repositories/raw/master</url>
        </repository>
    </repositories>



    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>

        <!-- framework版本 -->
        <framework-core.version>1.0.1</framework-core.version>
        <framework-common.version>1.0.6</framework-common.version>
        <microservice-provider.version>1.0.2</microservice-provider.version>
        <microservice-gateway.version>1.0.0</microservice-gateway.version>
        <framework-web.version>1.0.7</framework-web.version>
        <framework-datasource-plus.version>1.0.7</framework-datasource-plus.version>
        <framework-security.version>1.0.7</framework-security.version>
        <framework-elasticsearch.version>1.0.0</framework-elasticsearch.version>


        <!-- 子项目版本 Api-->
        <!-- message -->
        <foundation-message-api.version>1.0.0</foundation-message-api.version>
        <foundation-message-provider.version>1.0.0</foundation-message-provider.version>
        <!-- common -->
        <foundation-common-api.version>1.0.0</foundation-common-api.version>
        <foundation-common-provider.version>1.0.0</foundation-common-provider.version>

        <!-- 3rd -->
        <mybatis-plus.version>2.1.9</mybatis-plus.version>
    </properties>


    <dependencyManagement>
        <dependencies>

            <!-- 核心模块 - 包含: framework-common-->
            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-core</artifactId>
                <version>${framework-core.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-common</artifactId>
                <version>${framework-common.version}</version>
            </dependency>

            <!-- 服务提供模块 -->
            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>microservice-provider</artifactId>
                <version>${microservice-provider.version}</version>
            </dependency>

            <!-- 服务提供模块 -->
            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>microservice-gateway</artifactId>
                <version>${microservice-gateway.version}</version>
            </dependency>

            <!-- web模块 -->
            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-web</artifactId>
                <version>${framework-web.version}</version>
            </dependency>



            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-datasource-plus</artifactId>
                <version>${framework-datasource-plus.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-security</artifactId>
                <version>${framework-security.version}</version>
            </dependency>


            <dependency>
                <groupId>com.locals.framework</groupId>
                <artifactId>framework-elasticsearch</artifactId>
                <version>${framework-elasticsearch.version}</version>
            </dependency>

            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus</artifactId>
                <version>${mybatis-plus.version}</version>
            </dependency>

            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-support</artifactId>
                <version>${mybatis-plus.version}</version>
            </dependency>




            <!-- Api -->
            <dependency>
                <groupId>com.locals.foundation</groupId>
                <artifactId>foundation-message-api</artifactId>
                <version>${foundation-message-api.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.foundation</groupId>
                <artifactId>foundation-common-api</artifactId>
                <version>${foundation-common-api.version}</version>
            </dependency>

            <dependency>
                <groupId>com.locals.foundation</groupId>
                <artifactId>foundation-common-provider</artifactId>
                <version>${foundation-common-provider.version}</version>
            </dependency>

        </dependencies>
    </dependencyManagement>


    <distributionManagement>

        <repository>
            <id>localRepository</id>
            <!-- 本地的 locals-maven-repositories 目录，deploy时会打包到此目录-->
            <url>file:../locals-maven-repositories</url>
        </repository>

    </distributionManagement>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

    </dependencies>


</project>